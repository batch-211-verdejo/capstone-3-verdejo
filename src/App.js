import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Dashboard from './components/Dashboard';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Userdashboard from './components/Userdashboard';
import EditUser from './components/EditUser';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import './App.css';

function App() {

  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  
  const unsetUser = () => {
    localStorage.clear();
  }

  

  useEffect(()=>{
    fetch(`https://capstone-2-verdejo.onrender.com/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[]);

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/admin" element={<Dashboard/>}/>
              <Route path="/userslist" element={<Userdashboard/>}/>
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/editUser/:userId" element={<EditUser/>}/>
              <Route path="/editProduct/:productId" element={<EditProduct/>} />
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<Error/>}/>
            </Routes>
          </Container>
        </Router>
    </UserProvider>

  );
}

export default App;
