import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Userdashboard () {

    const {user} = useContext(UserContext);

    const [allUsers, setAllUsers] = useState([]);

    const fetchData = () => {

        fetch(`https://capstone-2-verdejo.onrender.com/users/userlist`, {
            headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
        })
        .then(res => res.json())
        .then(data => {

            setAllUsers(data.map(user => {
                return (
                    <tr key={user._id}>
                        <td>{user._id}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.userName}</td>
                        <td>{user.email}</td>                        
                        <td>{user.mobileNo}</td>
                        <td>{user.isAdmin ? "Admin" : "User"}</td>
                        <td>
                            {
                                (user.isAdmin)
                                ?
                                <Button variant="success" size="sm" onClick={() =>setAsUser(user._id, user.userName)} className="m-2">Set as User</Button>
                                :
                                <>
                                <Button variant="success" size="sm" onClick={() =>setAsAdmin(user._id, user.userName)} className="m-2">Set as Admin</Button>
                                <Button as={ Link } to={`/editUser/${user._id}`} variant="secondary" size="sm" className="m-2">Edit</Button>
                                </>
                            }
                            <Button variant="danger" size="sm" onClick={() => deleteUser(user._id, user.userName)} className="m-2">Delete</Button>
                        </td>
                    </tr>
                )
            }))
        })
    };

    const setAsAdmin = (userId, userName) => {

        fetch(`https://capstone-2-verdejo.onrender.com/users/${userId}/admin`,{
            method: "PUT",
            headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
        })
        .then(res => res.json())
        .then(data => {
            if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userName} role changed to admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Fail!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
        })
    };

    const setAsUser = (userId, userName) => {

        fetch(`https://capstone-2-verdejo.onrender.com/users/${userId}/admin`,{
            method: "PUT",
            headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
        })
        .then(res => res.json())
        .then(data => {
            if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userName} role changed to user.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Fail!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
        })
    };



    const deleteUser = (userId, userName) => {
		console.log(userId);
		console.log(userName);

		fetch(`https://capstone-2-verdejo.onrender.com/users/${userId}`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Deletion Successful!",
					icon: "success",
					text: `${userName} has been deleted from the list.`
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Product Deletion Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	};

    useEffect(() => {
        fetchData();
    })

    return (
        (user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>				

				<Button as={Link} to="/admin" variant="primary" size="lg" className="mx-2">Product Dashboard</Button>

			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>User ID</th>
		         <th>First Name</th>
		         <th>Last Name</th>
                 <th>User Name</th>
		         <th>Email</th>
		         <th>Mobile Number</th>
		         <th>Role</th>
                 <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
    )
}