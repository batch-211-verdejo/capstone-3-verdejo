import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function EditUser () {

    const {user} = useContext(UserContext);

    const { userId } = useParams();

    const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [userName, setUserName] = useState('');
	const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');    

    const [isActive, setIsActive] = useState(false);
    const navigate = useNavigate();
    

    function editUser(e) {

        e.preventDefault();

        fetch(`https://capstone-2-verdejo.onrender.com/users/${userId}`, {
            method: "PUT",
            headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    firstName: firstName,
			    lastName: lastName,
			    userName: userName,
				email: email,
                mobileNo: mobileNo                
			})
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
	    		Swal.fire({
	    		    title: "User succesfully Updated",
	    		    icon: "success",
	    		    text: `${email} credentials are now updated`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

        })
       

        setFirstName('');
        setLastName('');
        setUserName('');
        setEmail('');
        setMobileNo('');       
        
        
    }

    useEffect(() => {

    
        if(firstName !== "" && lastName !== "" && userName !== "" && email !== "" && mobileNo !== "" ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, userName, email, mobileNo]);

    useEffect(()=> {

    	

    	fetch(`https://capstone-2-verdejo.onrender.com/users/${userId}`)
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

    	
    		setFirstName(data.firstName);
            setLastName(data.lastName);
            setUserName(data.userName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);   		

    	});

    }, [userId]);


    return (
        user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Edit User</h1>
		        <Form onSubmit={(e) => editUser(e)}>
		        	<Form.Group controlId="firstName" className="mb-3">
		                <Form.Label>First Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter First Name" 
			                value = {firstName}
			                onChange={e => setFirstName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="lastName" className="mb-3">
		                <Form.Label>Last Name</Form.Label>
		                <Form.Control
		                	type="text"
		                	rows={3}
			                placeholder="Enter Last Name" 
			                value = {lastName}
			                onChange={e => setLastName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="username" className="mb-3">
		                <Form.Label>Username</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Username" 
			                value = {userName}
			                onChange={e => setUserName(e.target.value)}
			                required
		                />
		            </Form.Group>

					<Form.Group controlId="email" className="mb-3">
		                <Form.Label>Email</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Email Address" 
			                value = {email}
			                onChange={e => setEmail(e.target.value)}
			                required
		                />
		            </Form.Group>

                    <Form.Group controlId="mobileno" className="mb-3">
		                <Form.Label>Mobile Number</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Mobile Number" 
			                value = {mobileNo}
			                onChange={e => setMobileNo(e.target.value)}
			                required
		                />
		            </Form.Group>

		           
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/userslist" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/products" />
    )
}