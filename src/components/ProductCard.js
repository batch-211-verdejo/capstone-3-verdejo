
import { Link } from 'react-router-dom';
import { Card, Button} from 'react-bootstrap';

export default function ProductCard ({productProp}){

	

	let { productName, description, price, _id } = productProp;

	
	return(
		<Card className="bg-dark text-white d-inline-block ml-3 mr-3 mb-3 w-25">
			<Card.Body>
				<Card.Title>{productName}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>				
				<Button variant='secondary' as={Link} to={`/products/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
	)
}