import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import { FaSearch } from 'react-icons/fa';
import UserContext from '../UserContext';
import '../components/AppNavbar.css';

export default function AppNavbar(){
	
	const { user } = useContext(UserContext);

	return(
		<Navbar className='bg-dark' bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/"><img src="../../images/Pelican.jpg" alt="logo"/></Navbar.Brand>
			
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link className="text-white" as={Link} to="/">Home</Nav.Link>
	            <Nav.Link className="text-white" as={Link} to="/products">Products</Nav.Link>
	            
	            {(user.id !== null) ?
	            	<Nav.Link className="text-white" as={Link} to="/logout">Logout</Nav.Link>
	            	:
	            	<>
	            		<Nav.Link className="text-white" as={Link} to="/login">Login</Nav.Link>
	            		<Nav.Link className="text-white" as={Link} to="/register">Register</Nav.Link>
	            	</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
