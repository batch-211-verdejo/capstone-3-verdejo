import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Dashboard(){

	
	const {user} = useContext(UserContext);

	
	const [allProducts, setAllProducts] = useState([]);

	
	const fetchData = () =>{
		
		fetch(`https://capstone-2-verdejo.onrender.com/products/productlist`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>{product.isAvailable ? "Active" : "Inactive"}</td>
						<td>
							{
								
								(product.isAvailable)
								?	
								
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.productName)}>Archive</Button>
									
								:
									<>
										
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.productName)}>Unarchive</Button>
										
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
										<Button variant="danger" size="sm" className="m-2" onClick={() => deleteItem(product._id, product.productName)}>Delete</Button>
							
						</td>
					</tr>
				)
			}))

		})
	};

	
	const archive = (productId, productName) =>{		

		fetch(`https://capstone-2-verdejo.onrender.com/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAvailable: false
			})
		})
		.then(res => res.json())
		.then(data =>{			

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	};

	
	const unarchive = (productId, productName) =>{		

		fetch(`https://capstone-2-verdejo.onrender.com/products/${productId}/activate`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAvailable: true
			})
		})
		.then(res => res.json())
		.then(data =>{			

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	};

	const deleteItem = (productId, productName) => {
		console.log(productId);
		console.log(productName);

		fetch(`https://capstone-2-verdejo.onrender.com/products/${productId}/delete`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Deletion Successful!",
					icon: "success",
					text: `${productName} has been deleted from the list.`
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Product Deletion Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	};

	
	useEffect(()=>{
	
		fetchData();

	})
	

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>

				<Button as={Link} to="/userslist" variant="primary" size="lg" className="mx-2">Show users</Button>

			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
				 <th>Stock</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}