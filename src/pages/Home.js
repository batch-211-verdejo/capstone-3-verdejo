import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';
import Products from './Products';
// import ProductsTest from './ProductsTest';

export default function Home(){

	const data = {
		title: "Pelican",
		content: "Affordable accessories!",
		destination: "/products",
		label: "See Products"
	}

	return(
		<>
			<Banner bannerProp={data}/>			
			<Products/>
			
		</>
	)
}