import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products(){
	
	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);	


	useEffect(()=>{
		
	fetch('https://capstone-2-verdejo.onrender.com/products')
	.then(res=>res.json())
	.then(data=>{

		
	const productArr = (data.map(product => {
		
		return (

			<ProductCard productProp={product} key={product._id}/>
			)
		}))
		setProducts(productArr)
	})

	},[])

	
	return(
		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<h1>Products</h1>
			<div className='d-flex gap-3 flex-wrap'>
				{products}
			</div>
		</>
	)
}